package com.spring.boot.poi;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.w3c.dom.stylesheets.LinkStyle;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Export {

    public static void main(String[] args) throws IOException, InterruptedException {
//        oneSheet();
        multiSheet();
    }

    private static void oneSheet() throws IOException, InterruptedException {
        Workbook wb = new XSSFWorkbook(); //or new HSSFWorkbook();

        Sheet sheet = createSheet(wb, "sheet1");

        CountDownLatch countDownLatch = new CountDownLatch(2);

        new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                Row row = createRow(sheet, i);
                createCell(wb, row, 0, HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                createCell(wb, row, 1, HorizontalAlignment.CENTER_SELECTION, VerticalAlignment.BOTTOM);
                createCell(wb, row, 2, HorizontalAlignment.FILL, VerticalAlignment.CENTER);
                createCell(wb, row, 3, HorizontalAlignment.GENERAL, VerticalAlignment.CENTER);
                createCell(wb, row, 4, HorizontalAlignment.JUSTIFY, VerticalAlignment.JUSTIFY);
                createCell(wb, row, 5, HorizontalAlignment.LEFT, VerticalAlignment.TOP);
                createCell(wb, row, 6, HorizontalAlignment.RIGHT, VerticalAlignment.TOP);
            }
            countDownLatch.countDown();
        }, "t1").start();

        new Thread(() -> {
            for (int i = 100; i < 200; i++) {
                Row row = createRow(sheet, i);
                createCell(wb, row, 0, HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                createCell(wb, row, 1, HorizontalAlignment.CENTER_SELECTION, VerticalAlignment.BOTTOM);
                createCell(wb, row, 2, HorizontalAlignment.FILL, VerticalAlignment.CENTER);
                createCell(wb, row, 3, HorizontalAlignment.GENERAL, VerticalAlignment.CENTER);
                createCell(wb, row, 4, HorizontalAlignment.JUSTIFY, VerticalAlignment.JUSTIFY);
                createCell(wb, row, 5, HorizontalAlignment.LEFT, VerticalAlignment.TOP);
                createCell(wb, row, 6, HorizontalAlignment.RIGHT, VerticalAlignment.TOP);
            }
            countDownLatch.countDown();
        }, "t2").start();

        countDownLatch.await();
        System.out.println("写入文件开始");

        // Write the output to a file
        try (OutputStream fileOut = new FileOutputStream("one-sheet.xlsx")) {
            wb.write(fileOut);
        }
        wb.close();
        System.out.println("写入文件结束");
    }

    private static void multiSheet() throws IOException, InterruptedException {
        Workbook wb = new XSSFWorkbook(); //or new HSSFWorkbook();

        List<Sheet> sheetList = new ArrayList<>();

        for (int i = 1; i <= 10; i++) {
            sheetList.add(wb.createSheet("sheet" + i));
        }

        ExecutorService executorService = Executors.newFixedThreadPool(10);
        CountDownLatch countDownLatch = new CountDownLatch(10);

        for (int j = 0; j < 10; j++) {
            int finalJ = j;
            executorService.execute(() -> {
                Sheet sheet = sheetList.get(finalJ);
                for (int i = 0; i < 10000; i++) {
                    Row row = createRow(sheet, i);
                    createCell(wb, row, 0, HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                    createCell(wb, row, 1, HorizontalAlignment.CENTER_SELECTION, VerticalAlignment.BOTTOM);
                    createCell(wb, row, 2, HorizontalAlignment.FILL, VerticalAlignment.CENTER);
                    createCell(wb, row, 3, HorizontalAlignment.GENERAL, VerticalAlignment.CENTER);
                    createCell(wb, row, 4, HorizontalAlignment.JUSTIFY, VerticalAlignment.JUSTIFY);
                    createCell(wb, row, 5, HorizontalAlignment.LEFT, VerticalAlignment.TOP);
                    createCell(wb, row, 6, HorizontalAlignment.RIGHT, VerticalAlignment.TOP);
                }
                countDownLatch.countDown();
            });
        }

        executorService.shutdown();
        countDownLatch.await();
        System.out.println("写入文件开始");

        // Write the output to a file
        try (OutputStream fileOut = new FileOutputStream("multi-sheet.xlsx")) {
            wb.write(fileOut);
        }
        wb.close();
        System.out.println("写入文件结束");
    }

    private static void basic() throws IOException {
        Workbook wb = new XSSFWorkbook(); //or new HSSFWorkbook();

        Sheet sheet = createSheet(wb, "sheet1");
        Row row = createRow(sheet, 2);
        row.setHeightInPoints(30);
        createCell(wb, row, 0, HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
        createCell(wb, row, 1, HorizontalAlignment.CENTER_SELECTION, VerticalAlignment.BOTTOM);
        createCell(wb, row, 2, HorizontalAlignment.FILL, VerticalAlignment.CENTER);
        createCell(wb, row, 3, HorizontalAlignment.GENERAL, VerticalAlignment.CENTER);
        createCell(wb, row, 4, HorizontalAlignment.JUSTIFY, VerticalAlignment.JUSTIFY);
        createCell(wb, row, 5, HorizontalAlignment.LEFT, VerticalAlignment.TOP);
        createCell(wb, row, 6, HorizontalAlignment.RIGHT, VerticalAlignment.TOP);

        // Write the output to a file
        try (OutputStream fileOut = new FileOutputStream("xssf-align.xlsx")) {
            wb.write(fileOut);
        }
        wb.close();
    }

    private static synchronized Sheet createSheet(Workbook wb, String name) {
        return wb.createSheet(name);
    }

    private static synchronized Row createRow(Sheet sheet, int i) {
        return sheet.createRow(i);
    }

    private static void createCell(Workbook wb, Row row, int column, HorizontalAlignment halign, VerticalAlignment valign) {
        Cell cell = row.createCell(column);
        cell.setCellValue("Align It");
//        CellStyle cellStyle = wb.createCellStyle();
//        cellStyle.setAlignment(halign);
//        cellStyle.setVerticalAlignment(valign);
//        cell.setCellStyle(cellStyle);
    }
}
